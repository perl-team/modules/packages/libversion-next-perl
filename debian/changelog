libversion-next-perl (1.000-2) unstable; urgency=medium

  [ gregor herrmann ]
  * debian/copyright: change Copyright-Format 1.0 URL to HTTPS.
  * Remove Tim Retout from Uploaders. Thanks for your work!

  [ Alex Muntada ]
  * Remove inactive pkg-perl members from Uploaders.

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ gregor herrmann ]
  * debian/watch: use uscan version 4.

  [ Debian Janitor ]
  * Bump debhelper from old 9 to 12.
  * Set debhelper-compat version in Build-Depends.
  * Set upstream metadata fields: Bug-Submit.
  * Remove obsolete fields Contact, Name from debian/upstream/metadata
    (already present in machine-readable debian/copyright).

 -- Jelmer Vernooĳ <jelmer@debian.org>  Tue, 28 Jun 2022 22:02:43 +0100

libversion-next-perl (1.000-1.1) unstable; urgency=medium

  * Non maintainer upload by the Reproducible Builds team.
  * No source change upload to rebuild on buildd with .buildinfo files.

 -- Holger Levsen <holger@debian.org>  Tue, 14 Jun 2022 14:45:50 +0200

libversion-next-perl (1.000-1) unstable; urgency=medium

  * Team upload.

  [ Salvatore Bonaccorso ]
  * Update Vcs-Browser URL to cgit web frontend
  * debian/control: Use HTTPS transport protocol for Vcs-Git URI

  [ Lucas Kanashiro ]
  * Add debian/upstream/metadata
  * Import upstream version 1.000
  * Refresh d/docs
  * Update years of upstream copyright
  * Update Debian packaging copyright
  * Bump debhelper compatibility level to 9
  * Declare compliance with Debian policy 3.9.7
  * Mark package as autopkgtest-able

  [ gregor herrmann ]
  * Add debian/NEWS with a note about incompatible changes.

 -- Lucas Kanashiro <kanashiro.duarte@gmail.com>  Sun, 28 Feb 2016 18:18:50 -0300

libversion-next-perl (0.004-1) unstable; urgency=low

  [ Ansgar Burchardt ]
  * debian/control: Convert Vcs-* fields to Git.

  [ Alessandro Ghedini ]
  * Email change: Alessandro Ghedini -> ghedo@debian.org

  [ gregor herrmann ]
  * debian/control: update {versioned,alternative} (build) dependencies.

  [ Salvatore Bonaccorso ]
  * Change Vcs-Git to canonical URI (git://anonscm.debian.org)
  * Change search.cpan.org based URIs to metacpan.org based URIs

  [ Axel Beckert ]
  * debian/copyright: migrate pre-1.0 format to 1.0 using "cme fix dpkg-
    copyright"

  [ gregor herrmann ]
  * Strip trailing slash from metacpan URLs.

  [ Florian Schlichting ]
  * Import Upstream version 0.004
  * Bump year of upstream copyright
  * Ship CONTRIBUTING documentation
  * Bump dh compatibility to level 8 (no changes necessary)
  * Update stand-alone license paragraphs to commonly used version (not
    limiting Debian to GNU/Linux)
  * Declare compliance with Debian Policy 3.9.5
  * Add myself to uploaders and copyright

 -- Florian Schlichting <fsfs@debian.org>  Fri, 07 Mar 2014 21:50:41 +0100

libversion-next-perl (0.002-1) unstable; urgency=low

  * Initial Release. (Closes: #606855)

 -- Alessandro Ghedini <al3xbio@gmail.com>  Sun, 02 Jan 2011 22:28:26 +0100
